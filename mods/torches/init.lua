
--[[

Torch mod - formerly mod "Torches"
======================

(c) Copyright BlockMen (2013-2015)
(C) Copyright sofar <sofar@foo-projects.org> (2016)

This mod changes the default torch drawtype from "torchlike" to "mesh",
giving the torch a three dimensional appearance. The mesh contains the
proper pixel mapping to make the animation appear as a particle above
the torch, while in fact the animation is just the texture of the mesh.


License:
~~~~~~~~
(c) Copyright BlockMen (2013-2015)

Textures and Meshes/Models:
CC-BY 3.0 BlockMen
Note that the models were entirely done from scratch by sofar.

Code:
Licensed under the GNU LGPL version 2.1 or higher.
You can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License
as published by the Free Software Foundation;

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

See LICENSE.txt and http://www.gnu.org/licenses/lgpl-2.1.txt

--]]

local S = minetest.get_translator("torches")

minetest.register_node("torches:torch", {
	description = S("Torch"),
	drawtype = "mesh",
	mesh = "torch_floor.obj",
	inventory_image = "itb_torch.png",
	wield_image = "itb_torch.png",
	tiles = {{
		    name = "itb_torch_anim.png",
		    animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 1.2}
	}},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	floodable = true,
	liquids_pointable = false,
	light_source = 8,
	groups = {choppy=2, dig_immediate=3, flammable=1, attached_node=1, torch=1},
	sounds = sounds.wood,
	drop = "torches:torch",
	on_timer = nodes.placeholder_particles,
	after_dig_node = nodes.after_dig_node_breakable,
	selection_box = {
		type = "wallmounted",
		wall_bottom = {-1/8, -1/2, -1/8, 1/8, 2/16, 1/8},
	},
	on_place = function(itemstack, placer, pointed_thing)
		local under = pointed_thing.under
		local node = minetest.get_node(under)
		local def = minetest.registered_nodes[node.name]
		if def and def.on_rightclick and
			((not placer) or (placer and not placer:get_player_control().sneak)) then
			return def.on_rightclick(under, node, placer, itemstack,
				pointed_thing) or itemstack
		end

		-- Disallow placing torches in buildable_to nodes
		if def and def.buildable_to and node.name ~= "air" and node.name ~= "nodes:placeholder" then
			return itemstack
		end
		local above = pointed_thing.above
		local above_node = minetest.get_node(above)
		local above_def = minetest.registered_nodes[above_node.name]
		local metatbl = {}
		if above_def and above_def.buildable_to and above_node.name ~= "air" then
			if above_node.name ~= "nodes:placeholder" then
				return itemstack
			else
				local meta = minetest.get_meta(above)
				metatbl = meta:to_table()
			end
		end

		local wdir = minetest.dir_to_wallmounted(vector.subtract(under, above))
		local fakestack = itemstack
		if wdir == 0 then
			return itemstack
		elseif wdir == 1 then
			fakestack:set_name("torches:torch")
		else
			fakestack:set_name("torches:torch_wall")
		end

		itemstack = minetest.item_place(fakestack, placer, pointed_thing, wdir)
		minetest.get_meta(above):from_table(metatbl)
		itemstack:set_name("torches:torch")

		return itemstack
	end
})

minetest.register_node("torches:torch_wall", {
	description = S("Torch"),
	drawtype = "mesh",
	mesh = "torch_wall.obj",
	tiles = {{
		    name = "itb_torch_anim.png",
		    animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 1.2}
	}},
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	buildable_to = true,
	floodable = true,
	walkable = false,
	light_source = 8,
	groups = {choppy=2, dig_immediate=3, flammable=1, not_in_creative_inventory=1, attached_node=1, torch=1},
	sounds = sounds.wood,
	drop = "torches:torch",
	on_timer = nodes.placeholder_particles,
	after_dig_node = nodes.after_dig_node_breakable,
	selection_box = {
		type = "wallmounted",
		wall_side = {-1/2, -1/2, -1/8, -1/8, 1/8, 1/8},
	},
})

frame.register("torches:torch")
