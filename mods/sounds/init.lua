
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

--[[

    sounds.lua

--]]

sounds = {}

for _, v in pairs({
	{"dirt"},
	{"glass"},
	{"grass"},
	{"gravel"},
	{"metal"},
	{"sand"},
	{"snow"},
	{"stone", 1},
	{"wood"},
	{"leaves"},
	{"water"},
	{"cloth"},
}) do
	sounds[v[1]] = {
		footstep = {name = v[1] .. "_step", gain = 1.0},
		dig = {name = v[1] .. "_dig", gain = 1.0},
		dug = {name = v[1] .. "_dig", gain = 1.0},
		place = {name = v[1] .. "_dig", gain = 1.0},
	}
	if v[2] then
		sounds[v[1]].place = {name = v[1] .. "_place", gain = 1.0}
	end
end

-- fix these sounds up
sounds.glass.footstep = {name = "stone_step", gain = 1.0}
sounds.glass.place = {name = "stone_place", gain = 1.0}

local function trim(tbl)
	local secs = os.time()
	local slice = math.floor(secs / 2)
	-- trim old slices first
	for k, _ in pairs(tbl) do
		if k < slice - 6 then
			tbl[k] = nil
		end
	end
end

local player_env_sounds = {}

local function do_env_sound(pos, node, tbl, sound)
	local secs = os.time()
	local slice = math.floor(secs / 2)

	-- get center pos of 2x2 node complex
	local center = vector.apply(pos, function(a)
		return (math.floor(a / 2) * 2) + 0.5
	end)
	local chash = minetest.hash_node_position(vector.apply(center, math.floor))

	-- scan for recent sound from center
	for k, _ in pairs(tbl) do
		if tbl[k][chash] then
			return
		end
	end
	local spec = {
		pos = center,
		loop = false,
		gain = 0.3,
		max_hear_distance = 16
	}
	local box = boxes.find_box(pos)
	if box and box.name then
		local name = box.name
		spec.to_player = name
		local v = name .. " " .. sound
		if not player_env_sounds[slice] then
			player_env_sounds[slice] = {}
		end
		if not player_env_sounds[slice][v] then
			player_env_sounds[slice][v] = 1
		else
			if player_env_sounds[slice][v] > 16 then
				return
			end
			player_env_sounds[slice][v] = player_env_sounds[slice][v] + 1
		end
	end

	minetest.sound_play(sound, spec)

	if not tbl[slice] then
		tbl[slice] = {}
	end
	tbl[slice][chash] = 1
end

--
-- fire sounds
--
local fire_sound_tbl = {}
minetest.register_abm({
	label = "fire_sounds",
	nodenames = {"nodes:fire", "nodes:furnace_on"},
	neighbors = {"air"},
	interval = 1,
	chance = 4,
	catch_up = false,
	action = function(pos, node)
		do_env_sound(pos, node, fire_sound_tbl, "fire_fire")
	end,
})


--
-- flowing water sounds
--
local flw_sound_tbl = {}
minetest.register_abm({
	label = "flowing_water_sounds",
	nodenames = {"nodes:water_flowing", "nodes:river_water_flowing"},
	neighbors = {"air"},
	interval = 1,
	chance = 4,
	catch_up = false,
	action = function(pos, node)
		do_env_sound(pos, node, flw_sound_tbl, "water_flowing")
	end,
})

--
-- leaves sounds
--
local leaves_sound_tbl = {}
minetest.register_abm({
	label = "leaves_sounds",
	nodenames = {"group:leaves"},
	neighbors = {"air"},
	interval = 1,
	chance = 4,
	catch_up = false,
	action = function(pos, node)
		do_env_sound(pos, node, leaves_sound_tbl, "leaves")
	end,
})

--
-- mech electric sounds
--
local mech_sound_tbl = {}
minetest.register_abm({
	label = "mech_sounds",
	nodenames = {"group:mech"},
	neighbors = {"air"},
	interval = 1,
	chance = 4,
	catch_up = false,
	action = function(pos, node)
		do_env_sound(pos, node, mech_sound_tbl, "mech_humm_60hz")
	end,
})

--
-- housekeeping timer
--
local function player_env_sounds_trim()
	trim(player_env_sounds)
	trim(fire_sound_tbl)
	trim(flw_sound_tbl)
	trim(leaves_sound_tbl)
	trim(mech_sound_tbl)
	minetest.after(2.0, player_env_sounds_trim)
end
minetest.after(2.0, player_env_sounds_trim)
