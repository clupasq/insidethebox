
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

queue = {}

local function update_queue()
	local q = {}
	for id = 0, db.get_last_box_id() do
		local meta = db.box_get_meta(id)
		if (meta.type == db.BOX_TYPE and meta.meta) then
			if meta.meta.status == db.STATUS_SUBMITTED then
				q[#q + 1] = {
					id = id,
					builder = meta.meta.builder,
					name = meta.meta.box_name
				}
			end
		end
	end
	queue = q

	local json = "{}"
	if #q > 0 then
		json = minetest.write_json(q)
	end
	local o = io.open(minetest.get_worldpath().."/queue.json", "w")
	if o then
		o:write(json)
		o:close()
	end

	minetest.after(15*60, update_queue)
end

minetest.after(1, update_queue)

minetest.register_chatcommand("queue", {
	description = "Display box review queue",
	func = function(name, param)
		local m

		if #queue == 0 then
			m = "There are no boxes in the queue"
		elseif #queue == 1 then
			m = "There is 1 box in the queue"
		else
			m = "There are " .. #queue .. " boxes in the queue"
		end

		if minetest.get_player_privs(name, {server = true}) then
			for k, v in pairs(queue) do
				m = m .. "\n" .. v.id .. ": \"" .. v.name .. "\" by " .. v.builder
			end
		end

		return true, m
	end
})

